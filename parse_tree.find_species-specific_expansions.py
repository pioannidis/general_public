#!/usr/bin/python3

import os
import sys
from ete3 import Tree

if len( sys.argv ) != 3 or os.path.exists( sys.argv[1] ) == False:
  directories = sys.argv[0].split( "/" )
  print( "Usage: %s <newick_file> <minimum size of copies>" % directories[-1] )
  sys.exit(1)

newick_file = sys.argv[1]

MINIMUM_SIZE = int( sys.argv[2] )

t = Tree(newick_file, format=1)

for node in t.traverse("postorder"):
  if not node.is_leaf(): # if the node is not a leaf node
    #print ("Node name: " + node.name)
    
    # the following dict will contain the species (i.e. leaves) of each node
    node_species = {}
    
    # the following list will contain the names of the genes in this node
    node_leaves = []
    
    for leaf in node: # for each leaf that is found...
      # add the gene name in the list
      node_leaves.append( leaf.name )
      
      # each leaf name follows the pattern XXX_YYY
      # where XXX is the species name and YYY is the gene name
      leafname_components = leaf.name.split( "_" )

      if leafname_components[0] in node_species:     # if the species was found before in this node...
        node_species[ leafname_components[0] ] += 1  # ...then increment its count by 1
      else:                                          # else
        node_species[ leafname_components[0] ] = 1   # set it equal to 1

    #print ( node_species )
    species_nr = len(node_species)
    if species_nr == 1:
      species_list = list ( node_species.keys() )
      gene_nr = node_species[ species_list[0] ]
      if gene_nr >= MINIMUM_SIZE:
        print ( "Node_" + node.name, end="\t" )
        #print ( node_species )
        print ( '\t'.join (node_leaves) )
