#!/usr/bin/perl -w
use warnings;
use strict;

if ( scalar(@ARGV) != 1 ) {
	print "USAGE: $0 <fasta file to convert>\n\n";
	exit;
}

open ( X, $ARGV[0] ) or die;
	$/ = "\n>";
	my @lines = <X>;
	close (X);
	foreach my $line (@lines) {
		$line=~s/>//g;
		my @n = split ( /\n/, $line );
		print ">".$n[0]."\n";
		foreach my $m (1..$#n) { print $n[$m]; }
		print "\n";
	}
