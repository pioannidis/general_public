#!/usr/bin/perl -w

use strict;
use warnings;

#if ( scalar (@ARGV) != 1 ) { die "USAGE: $0 <m8 blast output>\n\n"; }

#open ( BLAST, $ARGV[0] ) or die;

#open ( REMOVED, ">removed.blastn" ) or die;

my $hsp = <>;

my $hsps_removed = 0;

#while ( defined ($hsp) ) {
while ( defined $hsp ) {
	print $hsp;
	
	my @f = split ( /\t/, $hsp );
	
	my $q_name = $f[0];
	
	while ( defined ($hsp) && $q_name eq $f[0] ) {
		#$hsp = <BLAST>;
		$hsp = <>;
		if ( defined ($hsp) ) {
			@f = split ( /\t/, $hsp );
			
			# count the number of hsps that are removed
			if ( $q_name eq $f[0] ) {
			#	print REMOVED $hsp;
				$hsps_removed++;
			}
		}
	}
}

print STDERR "$hsps_removed hsps were removed\n\n";
