# General purpose scripts used in various projects

This git projects serves as a repository for the scripts I have used in various projects.


## Usage - edit_seq_in_one_line.pl

```
edit_seq_in_one_line.pl <fasta file>
```
This script puts the sequence of multi-fasta files in one line only.


## Usage - parse_m8_blast_keep_best_hsp_per_query.pl
```
parse_m8_blast_keep_best_hsp_per_query.pl < <BLAST output (fmt6 format)>
```
This script keeps only the first hit for each query.


## Usage - remove_sequences_from_fasta.pl
```
remove_sequences_from_fasta.pl <fasta file> <list of names to be excluded from the fasta file>
```
This script reads in a file with names (second file) that should be excluded from the fasta file (first file).


## Usage - get_lineage_from_taxon_id.pl

```
get_lineage_from_taxon_id.pl <fmt6 BLAST output, containing the taxon ID> <local taxon ID cache>
```

This script takes a BLAST output (tab-delimited - fmt6 format) as input and adds the taxonomic lineage of the hit (i.e. of the subject sequence). The BLAST output file should contain the NCBI taxon ID, because it is used as query in the NCBI Taxonomy to retrieve the taxonomic lineage. It is expected to be present in the penultimate field, but you can change this, by modifying the $TAXON_FIELD variable, inside the script. The script depends on the Bio::DB::Taxonomy module. The second argument which is essentially a plain text, tab-delimited file that serves as a cache to speed up the whole procedure; you can use the supplied `local.db`, which will grow in size after each time you've run the script.


## Usage - parse_tree.find_species-specific_expansions.py

```
parse_tree.find_species-specific_expansions.py <newick_file> <minimum size of copies>
```

This script takes a phylogenetic tree in Newick format and outputs all clades of the tree that contain genes of one species only. Obviously, for this to work you have to have included this information in the sequences you used for building the tree, in the first place. For example, your Newick tree should look something like this:

```
((Tni_CYP341B41,(Harmi_CYP341B9,(Sexig_CYP341B18,(Sfrug_CYP341B18,Sfrug_CYP341B19)))),((Bmori_CYP341B1,Msext_CYP341B12),Egris_CYP341B63))
```

In the example above the species name is the first part of the taxon name (e.g. "Tni", "Harmi", "Sexig" etc). Also in the above example, the species-specific expansion that will be reported by this script would be the Sfrug one.


## Usage - calc_mean_stdev.v2.py 

```
calc_mean_stdev.v2.py <tab-delimited expression values>
```

The input file should look like this:
```
Name	day1-101c-1	day1-101c-2	day1-101c-4	day1-102c-1	day1-102c-2	day1-102c-3	day1-102c-4
Pxylo_CYP15C1	9.198	7.608	8.477	7.954	8.086	8.811	8.887
Pxylo_CYP18A1	12.058	12.190	12.292	12.932	11.876	12.320	12.342
Pxylo_CYP18B1	0.000	0.000	0.000	4.910	0.000	0.000	0.000
```

This script will calculate the mean and stdev for each condition
(any number of replicates for each condition).
It will then print the result in the stdout.


## Usage - parse_vcf.count_effects.py

This script will take an annotated VCF file and count the number of occurrences of each variant. I have used it with VCF files annotated with SnpEff.

```
usage: parse_vcf.count_effects.py [-h] -i INVCF [-v]

This script will take an annotated VCF file as input and count
the number of occurrences of each effect (aka annotation).
The output will be a tab-delimited file containing the name of
the effect in the first field and the number of its occurrences
in the second field.

options:
  -h, --help            show this help message and exit
  -i INVCF, --invcf INVCF
                        annotated VCF file
  -v, --version         show program's version number and exit
```

The **annotated** input VCF file should look like this:
```
##SnpEffVersion="5.2a (build 2023-10-24 14:24), by Pablo Cingolani"
##SnpEffCmd="SnpEff  -noStats GRCh38.105 NA19239.vcf "
##INFO=<ID=ANN,Number=.,Type=String,Description="Functional annotations: 'Allele | Annotation | Annotation_Impact | Gene_Name | Gene_ID | Feature_Type | Feature_ID | Transcript_BioType | Rank | HGVS.c | HGVS.p | cDNA.pos / cDNA.length | CDS.pos / CDS.length | AA.pos / AA.length | Distance | ERRORS / WARNINGS / INFO' ">
##INFO=<ID=LOF,Number=.,Type=String,Description="Predicted loss of function effects for this variant. Format: 'Gene_Name | Gene_ID | Number_of_transcripts_in_gene | Percent_of_transcripts_affected'">
##INFO=<ID=NMD,Number=.,Type=String,Description="Predicted nonsense mediated decay effects for this variant. Format: 'Gene_Name | Gene_ID | Number_of_transcripts_in_gene | Percent_of_transcripts_affected'">
chrY	10693871	.	G	TTCCACTCCACTCCACTCCATTCTCCTTAAGTTTCCACTACTTCGCAGCTTCGAAGTGCGTCGTATTGGGCACCCCNNNNNNNNNNCCCC	.	PASS	DP=100;ANN=TTCCACTCCACTCCACTCCATTCTCCTTAAGTTTCCACTACTTCGCAGCTTCGAAGTGCGTCGTATTGGGCACCCCNNNNNNNNNNCCCC|intergenic_region|MODIFIER|RNA5-8SP6-ENSG00000273858|ENSG00000251705-ENSG00000273858|intergenic_region|ENSG00000251705-ENSG00000273858|||n.10693871delGinsTTCCACTCCACTCCACTCCATTCTCCTTAAGTTTCCACTACTTCGCAGCTTCGAAGTGCGTCGTATTGGGCACCCCNNNNNNNNNNCCCC||||||	NODE_3_length_2393_cov_5.359240	267	321	2051	2122
[...more variants...]
```

So, you can run it like this:
```
parse_vcf.count_effects.py -i NA19143.ann.vcf > NA19143.ann.vcf.effects_count
```

And the output will look like this:
```
#Effect	Count	Fraction
intron_variant	249	0.321
upstream_gene_variant	133	0.171
intergenic_region	294	0.379
downstream_gene_variant	90	0.116
3_prime_UTR_variant	4	0.005
non_coding_transcript_exon_variant	3	0.004
frameshift_variant&stop_gained&synonymous_variant	1	0.001
splice_acceptor_variant&intron_variant	1	0.001
frameshift_variant&synonymous_variant	1	0.001
```

## Support
You can send me a message here, or email me.
