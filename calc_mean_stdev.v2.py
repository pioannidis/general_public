#!/usr/bin/python3

import os
import getopt, sys
import re
import statistics


def uniq( in_list ):

  uniq_list = []

  uniq_dict = set( in_list )

  for x in uniq_dict:
    uniq_list.append( x )

  return uniq_list



if len( sys.argv ) != 2 or os.path.exists( sys.argv[1] ) == False:
  directories = sys.argv[0].split( "/" )
  print( 
"""
Usage: %s <tab-delimited expression values>

The input file should look like this:
---------------
Name	day1-101c-1	day1-101c-2	day1-101c-4	day1-102c-1	day1-102c-2	day1-102c-3	day1-102c-4
Pxylo_CYP15C1	9.198	7.608	8.477	7.954	8.086	8.811	8.887
Pxylo_CYP18A1	12.058	12.190	12.292	12.932	11.876	12.320	12.342
Pxylo_CYP18B1	0.000	0.000	0.000	4.910	0.000	0.000	0.000
---------------

This script will calculate the mean and stdev for each condition
(any number of replicates for each condition).
It will then print the result in the stdout.

""" % directories[-1] )
  sys.exit(1)


in_file = sys.argv[1]

fh = open ( in_file ) # open the file

conds_inv = {} # same as above, but different! See below.

for line in fh:
  line = line.strip()
  data = line.split( "\t" )
  if data[0] == "Name":
    # This is the header of the file.
    # We have to extract the conditions from the header
    # (e.g. "day1-101c", "day1-102c", etc)
    for datum in data:
      if datum == "Name":
        next
      else:
        cond = re.sub( r"-\d$", "", datum )   # get the name of the condition by removing the replicate number
        # for the conds_inv dict do the inverse;
        # <condition> --> <list of indices that belong to the same condition>
        if cond in conds_inv:
          conds_inv[cond].append( data.index(datum) )
        else:
          conds_inv[cond] = []                         # initialize...
          conds_inv[cond].append( data.index(datum) )  # ...and add the values 
      
  else:
    # These are the lines containing the expression values
    gene = data[0]
    #print ( "#" + gene )
    results = [] # this will hold the final results
                 # I'm putting them in a list so that
                 # I can sort them before printing them out
    cond = ""
    # For each condition in conds_inv
    for cond in conds_inv:
      # conds_inv[cond] is a list of the field numbers (indices)
      # of the replicates that belong to the same condition
      # Thus, the first thing you have to do is loop through
      # these indices and create a list that contains
      # the expression values of each replicate
      cond_data = []
      for i in conds_inv[cond]:
        cond_data.append( float(data[i]) )

      # calculate the mean and stdev for the sub-list above
      expr_mean = statistics.mean ( cond_data )
      expr_mean = round( expr_mean, 3 )
      expr_stdev = statistics.stdev ( cond_data )
      expr_stdev = round( expr_stdev, 3 )

      # reformat the condition so that it is
      # treatment-day, instead of day-treatment
      # i.e. 101c-day1, instead of day1-101c
      tmp = cond.split( "-" )
      cond_reform = tmp[1] + "-" + tmp[0]
      
      # generate the line
      out_line = cond_reform + "\t" + str(expr_mean) + "\t" + str(expr_stdev)
      # and add it to the results list
      results.append( out_line )

    # now that you finished looping through all the
    # conditions for the given gene, sort the results
    results = sorted (results)
    
    # ...open a file
    out_file = open ( gene + ".tsv", "w" )
    
    # ...and write the results (condition name, mean, stdev)
    out_file.write( "condition\tmean\tsd\n" ) # print the header
    
    for line in results:
      out_file.write(line + "\n")

    out_file.close()

#print (conds_inv)
