#!/usr/bin/env python3
# 
#
#

import os
import argparse
import sys

def uniq( in_list ):
	uniq_list = []
	uniq_dict = set( in_list )
	for x in uniq_dict:
		uniq_list.append( x )
	return uniq_list


def parse_input():
	# create an argument parser
	parser = argparse.ArgumentParser( formatter_class=argparse.RawDescriptionHelpFormatter,
					  description = "This script will take an annotated VCF file as input and count\nthe number of occurrences of each effect (aka annotation).\nThe output will be a tab-delimited file containing the name of\nthe effect in the first field and the number of its occurrences\nin the second field.",
					  epilog = "And this is the epilog." )
	
	# positional mandatory arguments
	parser.add_argument( "-i", "--invcf", help = "annotated VCF file", required = True, type = str )
		
	# optional arguments
	#parser.add_argument( "-o", "--suffix", help = "Suffix for the output files, default = 'mod'", default = "mod", type = str )

	# print version
	parser.add_argument( "-v", "--version", action = "version", version = '%(prog)s - version 1.0' )
	
	# parse arguments
	args = parser.parse_args()

	return args

#
#
# main script starts here!


if len( sys.argv ) == 1:
	sys.stderr.write( "No arguments found. Run with '-h'/'--help' for details\n" )
	sys.exit(1)

args = parse_input()


# check if a genome file exists. If it doesn't exist then the script should die.
if os.path.exists( args.invcf ) == False:
	sys.stderr.write ( "ERROR: VCF file " + args.invcf + " doesn't exist. Exiting...\n" )
	sys.exit(1)

sys.stderr.write( "INFO: Opening VCF file: " + args.invcf + "\n" )
fh1 = open ( args.invcf )

line = fh1.readline()

counter = {} # this is the dict that will be counting the effects found in the VCF file

variant_nr = 0  # this is counting the total number of variants in the VCF file

while True:
	line = line.strip()
	if line.startswith("##"):
		line = fh1.readline()
		continue
	variant_nr += 1 # if the line isn't a comment,
			# then increment the variant counter
	f = line.split( "\t" )
	
	# the effects are found in the 2nd sub-field of the INFO field (the 8th field of a VCF file)
	ff = f[7].split( "|" )
	effect = ff[1]
	
	if effect in counter:
		counter[effect] += 1
	else:
		counter[effect] = 1
	
	line = fh1.readline()
	if not line:
		break
fh1.close()

#print ( "Number of variants " + str( variant_nr ) )

# print the header
print( "#Effect\tCount\tFraction" )
for effect in counter:
	fraction = round( counter[effect] / variant_nr, 3 )
	
	print( effect + "\t" + str( counter[effect] ) + "\t" + str( fraction ) )
