#!/usr/bin/perl -w

use warnings;
use strict;

if ( scalar (@ARGV) != 1 ) { die "USAGE: $0 <Orthogroups.GeneCount.tsv>\n\n"; }
# The "Orthogroups.GeneCount.tsv" file looks like this

#Orthogroup	Aratha	Cersil	Glymax	Maldom	Medtru	Orysat	Phavul	Pissat	Sollyc	Triaes	Zeamay
#OG0000000	0	867	8	3	2	2	0	24	15	1	0
#OG0000001	8	41	50	49	79	17	64	25	15	37	13
#OG0000002	0	347	1	10	2	1	0	9	21	1	2
#OG0000003	0	388	0	0	0	0	0	0	1	0	0
#OG0000004	0	236	38	4	3	31	3	12	1	25	32
#OG0000005	0	0	0	0	0	0	0	355	0	0	0
#OG0000006	0	2	0	0	0	0	0	349	1	0	0
#OG0000007	1	34	34	74	15	7	2	87	55	8	8

open ( IN, $ARGV[0] ) or die;

my $line = <IN>; # species names are contained in the first line
chomp ($line);

my @species_list = split( "\t", $line );
shift ( @species_list );

my @sc_all = ();    # single-copy in all species
my @pr_all = ();    # present-in-all species
my @pr_maj = ();    # present in the majority (except at most 2 species)
my @pr_pat = ();    # patchy distribution (>=4 species)
my @pr_oth = ();    # not in any of the above categories (in two or three species)
my @pr_ssp = ();    # species-specific (only one species)
my @pr_fab = ();    # Fabaceae-only, in all Fabaceae, >=1 copies, specific to the Carob project

while ( $line = <IN> ) {
	chomp ($line);
	
	my @counts = split ( /\t/, $line );
	
	my $orthogroup = shift ( @counts );
	#print STDERR "orthogroup = $orthogroup\n";

	# count the number of species having a single gene copy in this orthogroup
	my $sc_sum = 0;
	foreach my $count ( @counts ) {
		if ( $count == 1 ) {
			$sc_sum++;
		}
	}
	#print STDERR "sc_sum = $sc_sum\n";


	# count the number of species with ANY number of genes (present)
	my $pr_sum = 0;
	foreach my $count ( @counts ) {
		if ( $count >= 1 ) {
			$pr_sum++;
		}
	}
	#print STDERR "pr_sum = $pr_sum\n";

	# count the number of Fabaceae species with ANY number of genes (present)
	# Fabaceae have indices
	# 1: Cersil
	# 2: Glymax
	# 4: Medtru
	# 6: Phavul
	# 7: Pissat
	my $pr_fab = 0;
	for my $i  ( 1, 2, 4, 6, 7 ) {
		if ( $counts[$i] >= 1 ) {
			$pr_fab++;
		}
	}


	if ( $pr_fab == 5 && $pr_sum == 5 ) { # if this OG is only present in Fabaceae...
		my $all_counts = "$orthogroup\t";
		$all_counts .= join ( "\t", @counts );
		push ( @pr_fab, $all_counts );
	}
	elsif ( $sc_sum == scalar ( @species_list ) ) { # if all species have a single-copy in this OG
		my $all_counts = "$orthogroup\t";
		$all_counts .= join ( "\t", @counts );
		push ( @sc_all, $all_counts );
	}
	elsif ( $pr_sum == scalar ( @species_list ) ) {
		# put all species counts in a single string
		my $all_counts = "$orthogroup\t";
		$all_counts .= join ( "\t", @counts );
		push ( @pr_all, $all_counts );
	}
	elsif ( scalar ( @species_list ) - $pr_sum <= 2 ) {
		my $all_counts = "$orthogroup\t";
		$all_counts .= join ( "\t", @counts );
		push ( @pr_maj, $all_counts );
	}
	elsif ( $pr_sum >= 4 ) {
		my $all_counts = "$orthogroup\t";
		$all_counts .= join ( "\t", @counts );
		push ( @pr_pat, $all_counts );
	}
	elsif ( $pr_sum == 1 ) {
		my $all_counts = "$orthogroup\t";
		$all_counts .= join ( "\t", @counts );
		push ( @pr_ssp, $all_counts );
	}
	else {
		my $all_counts = "$orthogroup\t";
		$all_counts .= join ( "\t", @counts );
		push ( @pr_oth, $all_counts );
	}
}

# loop through each array and count the number of genes for each species

########################### SC_ALL #########################
my @counters = ();

print "\t", join ( "\t", @species_list ), "\n";

foreach my $tmp (@sc_all) {
	my @f = split ( /\t/, $tmp );
	shift (@f);
	for ( my $i = 0; $i < scalar (@f); $i++ ) {
		$counters[$i] += $f[$i];
	}
}
print "SC_ALL\t", join ( "\t", @counters ), "\n";

############################ PR_ALL ########################
@counters = ();

foreach my $tmp (@pr_all) {
	my @f = split ( /\t/, $tmp );
	shift (@f);
	for ( my $i = 0; $i < scalar (@f); $i++ ) {
		$counters[$i] += $f[$i];
	}
}
print "PR_ALL\t", join ( "\t", @counters ), "\n";

########################### PR_MAJ ##########################
@counters = ();

foreach my $tmp (@pr_maj) {
	my @f = split ( /\t/, $tmp );
	shift (@f);
	for ( my $i = 0; $i < scalar (@f); $i++ ) {
		$counters[$i] += $f[$i];
	}
}
print "PR_MAJ\t", join ( "\t", @counters ), "\n";


######################## PATCHY ############################
@counters = ();

foreach my $tmp (@pr_pat) {
	my @f = split ( /\t/, $tmp );
	shift (@f);
	for ( my $i = 0; $i < scalar (@f); $i++ ) {
		$counters[$i] += $f[$i];
	}
}
print "PR_PAT\t", join ( "\t", @counters ), "\n";


######################## PR_OTH ############################
@counters = ();

foreach my $tmp (@pr_oth) {
	my @f = split ( /\t/, $tmp );
	shift (@f);
	for ( my $i = 0; $i < scalar (@f); $i++ ) {
		$counters[$i] += $f[$i];
	}
}
print "PR_OTH\t", join ( "\t", @counters ), "\n";

######################## LIN_SP ############################
@counters = ();

foreach my $tmp (@pr_ssp) {
	my @f = split ( /\t/, $tmp );
	shift (@f);
	for ( my $i = 0; $i < scalar (@f); $i++ ) {
		$counters[$i] += $f[$i];
	}
}
print "PR_SSP\t", join ( "\t", @counters ), "\n";


######################## PR_FAB ############################
@counters = ();

foreach my $tmp (@pr_fab) {
	my @f = split ( /\t/, $tmp );
	shift (@f);
	for ( my $i = 0; $i < scalar (@f); $i++ ) {
		$counters[$i] += $f[$i];
	}
}
print "PR_FAB\t", join ( "\t", @counters ), "\n";


