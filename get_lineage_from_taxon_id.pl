#!/usr/bin/perl -ws
#use strict;
#use warnings;
use Bio::DB::Taxonomy;
my $db = Bio::DB::Taxonomy->new(-source => 'entrez');

#$| = 1;

if ( scalar (@ARGV) != 2 ) {
        die "USAGE: $0 <fmt6 blast output (tab-delimited)> <local db>\n\n";
}

# load the taxon IDs-to-lineage from the local database to save time.

# the local db is loaded in cache. Also, the taxon IDs during execution
# are loaded in cache to speed up the process. At the end of the run,
# the entire cache is saved as the local db.

print STDERR "Loading local.db...\n";

my %cache = ();

open ( LOCAL_DB, $ARGV[1] ) or die;

while ( my $line = <LOCAL_DB> ) {
	chomp ($line);
	# each line should contain two fields; taxon ID and lineage
	my @f = split ( /\t/, $line );
	$cache{$f[0]} = $f[1];
}
close (LOCAL_DB);

print STDERR "Done loading local.db.\n\n";

my $TAXON_FIELD = -2; # this is for a BLAST output
#my $TAXON_FIELD = 0; # this is for a list of taxon IDs (not a BLAST output!)

my $UPDATE_FREQ = 10; # it will print an update after this many lines have been processed

# Before looping the BLAST output, count the total lines so that you can later
# give an estimate of the progress (since these files can be really big)
my $no_lines = `wc -l $ARGV[0]`;
( $no_lines ) = ( $no_lines =~ /^(\d+) / );

open ( BLAST, $ARGV[0] ) or die;

while ( my $line = <BLAST> ) {
	# Progress message
	if ( $. % $UPDATE_FREQ == 0 ) {
		print STDERR "\rProcessed $. / $no_lines lines...";
	}
	
	#my $line = $_;
	chomp $line;
	# tabular BLAST output with the taxon ID in the $TAXON_FIELD field
	my @f = split (/\t/, $line);
	#print STDERR $f[-1], "\n";
	
	# use NCBI Entrez over HTTP
	#my $taxonid = $db->get_taxonid($f[0]);
	my $taxonid = $f[$TAXON_FIELD];

	# First look in cache
	if ( exists ($cache{$taxonid}) ) {
		splice ( @f, -1, 0, $cache{$taxonid} );
		print join ( "\t", @f), "\n";
		next; # take the next line
	}
	
	if ( $taxonid eq "00000" ) {
		# if no taxon ID was found for this sequence...
		splice ( @f, -1, 0, "No taxon ID was found for this sequence" );
		print join ( "\t", @f), "\n";
		next;
	}
	elsif ( $taxonid eq "1" ) {
		# this is the root, for which there's no lineage and the script hangs
		# when it tries to find it.
		splice ( @f, -1, 0, "root" );
		print join ( "\t", @f), "\n";
		next;
	}
	elsif ( $taxonid eq "1652077" ) {
		# this was Collembola, but it has been deleted from NCBI Taxonomy
                splice ( @f, -1, 0, "Collembola_Deleted" );
                print join ( "\t", @f), "\n";
                next;
	}

	# get a taxon
	my $taxon = $db->get_taxon(-taxonid => $taxonid);

	if(!$taxon) {
		print STDERR "Couldn't find taxonomy info for $f[$TAXON_FIELD]\n";
		$retval = {
			'acc'=> $acc,
			'gi'=> $gi,
			'taxon_id'=> $taxonid
		};
		splice ( @f, -1, 0, "No lineage information" );
		
		print join ( "\t", @f), "\n";
		
		# add this to the cache
		$cache{$taxonid} = "No lineage information";
	}
	elsif ($taxon->isa('Bio::Taxon')) {
		my $name = $taxon->scientific_name;
		my $c = $taxon;
		my @lineage = ($name);
		while (my $parent = $db->ancestor($c)) {
			unshift @lineage, $parent->scientific_name;
			$c = $parent;
		}
		$retval = {
			'gi' => $gi,
			'acc'=> $acc,
			'taxon_id'=> $taxonid, 
			'name'=> $name, 
			'lineage'=> join("; ", @lineage)
		};
		splice ( @f, -1, 0, $retval->{lineage} );

		print join ( "\t", @f ), "\n";
		
		# add this to the cache
		$cache{$taxonid} = $retval->{lineage};
	}
	else {
		print STDERR "Had something other than a Bio::Taxon\n";
	}
}

# Write the cache contents as a local db to speed up future runs
open ( OUT, ">$ARGV[1]" ) or die;

foreach my $tmp (sort keys %cache ) {
	print OUT "$tmp\t$cache{$tmp}\n";
}
close (OUT);

