#!/usr/bin/perl -w

use strict;

if ( scalar (@ARGV) < 2 ) {
	die "\nUSAGE: $0 <fasta file> <list of names to exclude>\n\n";
}

open ( LIST, $ARGV[1] ) or die;

my %list = ();

while ( my $name = <LIST> ) {
	chomp ($name);
	$list{$name} = 1;
}

close (LIST);


$/ = "\n>";

open ( FASTA, $ARGV[0] ) or die;

while ( my $seq = <FASTA> ) {
	chomp ($seq); # this removes all "\n>"
	$seq =~ s/>//g;
	my ( $header ) = ( $seq =~ /^(.+?)\n/ ); # non-greedy matching
	my ( $name ) = ( $header =~ /^(\S+)/ );

	$seq =~ s/^.+\n//;
	$seq =~ s/\n//g;
	
	if ( !$list{$name} ) {
		print ">$header\n$seq\n";
	}
	else {
		$list{$name} = 2;
	}
}

foreach my $tmp ( keys %list ) {
	if ( $list{$tmp} == 1 ) {
		print STDERR "Didn't remove $tmp because I didn't find it!\n\n";
	}
}
